# NoisePngImport

## How to use
* copy the addon to your project
* add .tres files containing NoiseTexture2D resources to the "noise_textures" array in "noise_png_import.gd"
* run NoisePngImport from Project -> Tools menu
