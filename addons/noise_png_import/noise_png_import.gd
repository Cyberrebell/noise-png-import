@tool
extends EditorPlugin

const noise_textures : Array[String] = [
]

func _enter_tree():
	add_tool_menu_item("NoisePngImport", update)

func _exit_tree():
	remove_tool_menu_item("NoisePngImport")

static func update():
	for texture_path in noise_textures:
		var tex : NoiseTexture2D = load(texture_path)
		tex.get_image().save_png(texture_path.replace(".tres", ".png"))
	print("NoisePngImport finished")
